package com.allstate.services;

import com.allstate.entities.Payment;
import com.allstate.exception.InvalidPaymentIdException;
import com.allstate.exception.InvalidPaymentRequestException;
import com.allstate.paymentDao.PaymentDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentDao dao;


    @Override
    public int rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id) {
        Payment payment;
        if(id>0){
            payment=dao.findByID(id);
        }else{
            throw new InvalidPaymentIdException("Payment id is not Valid.");
        }
        return payment;
    }

    @Override
    public List<Payment> findByType(String type)
    { List<Payment> paymentList =null;
    if(type!=null || !type.trim().equals("")){
        paymentList=dao.findByType(type);
    }else{
        throw new InvalidPaymentRequestException("Payment type should not be null or empty.");
    }
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }

    @Override
    public int update(Payment payment) {
        return dao.update(payment);
    }

}
