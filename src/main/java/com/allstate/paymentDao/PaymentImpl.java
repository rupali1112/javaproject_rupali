package com.allstate.paymentDao;

import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class PaymentImpl implements PaymentDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowcount() {
        List<Payment> paymentList = mongoTemplate.findAll(Payment.class);
        return paymentList.size();
    }

    @Override
    public Payment findByID(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payment = mongoTemplate.find(query, Payment.class);
        return payment;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return 1;
    }

    @Override
    public int update(Payment payment) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(payment.getId()));
        Update update = new Update();
        update.set("type", payment.getType());
        update.set("amount", payment.getAmount());
        update.set("custId", payment.getCustId());
        mongoTemplate.updateFirst(query, update, Payment.class);
        return 1;
    }
}
