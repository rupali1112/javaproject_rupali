package com.allstate.paymentDao;

import com.allstate.entities.Payment;

import java.util.List;

public interface PaymentDao {

    int rowcount();

    Payment findByID(int id);

    List<Payment> findByType(String type);

    int save(Payment payment);

    int update(Payment payment);
}
