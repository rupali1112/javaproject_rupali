package com.allstate.controller;

import com.allstate.entities.Payment;
import com.allstate.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/payments")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;
    Logger logger = Logger.getLogger(PaymentController.class.getName());

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        logger.info("Payment status method check");
        return "Payment Rest Application is running.";
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int rowcount() {
        logger.info("Inside rowcount method ");
        return paymentService.rowcount();
    }

    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findbyid(@PathVariable("id") int id) {
        logger.info("inside findById method");
        Payment payment = paymentService.findById(id);
        if (payment == null) {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List> findbytype(@PathVariable("type") String type) {
        logger.info("inside findByType method");
        List<Payment> paymentList = paymentService.findByType(type);

        if (paymentList.size() == 0) {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public String addPayment(@RequestBody Payment payment) {
        logger.info("Inside add payment method ");
        int flag = paymentService.save(payment);
        if (flag == 1) {
            return "Payment data saved successfully";
        } else {
            return "Payment failed";
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public String update(@RequestBody Payment payment) {
        logger.info("Inside update method");
        int updateFlag = paymentService.update(payment);
        if (updateFlag == 1) {
            return "Payment updated successfully";
        } else {
            return "Payment updation failed";
        }
    }

}
