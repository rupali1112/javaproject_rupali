package com.allstate.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidPaymentIdException extends RuntimeException {
    public InvalidPaymentIdException(String message)
    {
        super(message);
    }
}
