package com.allstate.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidPaymentRequestException extends RuntimeException {
    public InvalidPaymentRequestException(String message)
    {
        super(message);
    }
}
