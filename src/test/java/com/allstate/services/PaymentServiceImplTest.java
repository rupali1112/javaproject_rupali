package com.allstate.services;


import com.allstate.entities.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ComponentScan("com.allstate")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentServiceImplTest {

    @Autowired
    private PaymentServiceImpl paymentService;

    @Test
    public void save_findByIdTest() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Payment payment1 = new Payment(21, date, "BankTransfer", 1600, 2100);
        paymentService.save(payment1);
        date = paymentService.findById(21).getPaymentDate();
        assertEquals(21, paymentService.findById(21).getId());
        assertEquals("11/11/2020", formatter.format(date));

    }

    @Test
    public void findByTypeTest() {

        List<Payment> payment = paymentService.findByType("Cash");
        assertNotNull(payment);
    }

    @Test
    void rowCountTest() {
        assertEquals(3, paymentService.rowcount());
    }

    @Test
    public void updateTest() {
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "BankTransfer", 6000, 21);
        paymentService.update(payment1);
        assertEquals(21, paymentService.findById(1).getCustId());
    }


}
