package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Date;

public class PaymentEntityTest {

    private Payment payment;
    private Date date;

    @BeforeEach
    void setUp() {
        date = new Date();
        payment = new Payment(1, date, "Cash", 500, 11);
    }

    @Test
    void getId() {
        assertEquals(1, payment.getId());
    }


    @Test
    void getDate() {
        assertEquals(date, payment.getPaymentDate());
    }

    @Test
    void getType() {
        assertEquals("Cash", payment.getType());
    }

    @Test
    void getAmount() {
        assertEquals(500, payment.getAmount());
    }

    @Test
    void getCustId() {
        assertEquals(11, payment.getCustId());
    }
}
