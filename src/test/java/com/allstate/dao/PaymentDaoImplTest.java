package com.allstate.dao;

import com.allstate.entities.Payment;
import com.allstate.paymentDao.PaymentDao;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ComponentScan("com.allstate")
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentDaoImplTest {

    @Autowired
    private PaymentDao paymentDao;

    @Autowired
    MongoTemplate tpl;

    @Test
    public void saveAndFindByIdSuccess() {
        Date date = new Date();
        Payment payment = new Payment(6, date, "Cash", 800, 31);
        paymentDao.save(payment);
        assertEquals(payment.getId(), paymentDao.findByID(6).getId());
    }

    @Test
    public void findByTypeSuccess() {

        assertEquals(null, paymentDao.findByType("Checks"));

    }

    @Test
    void rowCountTest() {
        assertEquals(3, paymentDao.rowcount());
    }
}
