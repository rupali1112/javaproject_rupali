package com.allstate;

import com.allstate.entities.Payment;
import com.allstate.paymentDao.PaymentImpl;
import com.allstate.services.PaymentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class PaymentApplicationTests {
    @Test
    void contextLoads() {
    }

    @Autowired
    private TestRestTemplate restTemplate;

    int port = 8080;

    @Test
    public void restApiTest() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/payments/status",
                String.class)).contains("Payment Rest Application is running.");
    }

}
