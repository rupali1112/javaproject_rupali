package com.allstate.controller;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class PaymentControllerTest {

    @Autowired
    private PaymentController paymentController;

    @Test
    public void testRowCountIsNotNull() throws Exception {
        int rowcount = paymentController.rowcount();
        assertThat(rowcount).isNotNull();
    }

    @Test
    public void testSaveAndReturnTrue() throws Exception {
        Date date = new Date();
        Payment payment = new Payment(8, date, "UPI", 5200, 25);
        String paymentValidation = paymentController.addPayment(payment);
        assertEquals("Payment data saved successfully", paymentValidation);

    }

    @Test
    public void testFindByIdWhenValidIdGiven() {
        ResponseEntity<Payment> response = paymentController.findbyid(8);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getId()).isEqualTo(8);
        assertThat(response.getBody().getType()).isEqualTo("UPI");
        assertThat(response.getBody().getAmount()).isEqualTo(5200);
    }

    @Test
    public void testFindByIdWhenValidIdNotGiven() {
        ResponseEntity<Payment> response = paymentController.findbyid(0);
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void testFindByTypeWhenTypeIsNotNull() {
        ResponseEntity<List> response = paymentController.findbytype("Cash");
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void testFindByTypeWhenTypeIsNull() {
        ResponseEntity<List> response = paymentController.findbytype(" ");
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

}
